#include <iostream>
using namespace std;

class SomeObject {
	int value;
public:
	SomeObject(int _value) : value(_value) {}
	void set(int _value) {value = _value;}
	int get() {return value;}
};

template <typename T>
class Item {
public:
	T value;
	Item<T>* prev;
	Item<T>* next;

	Item(T _value, Item* _prev = nullptr, Item* _next = nullptr) {
		value = _value;
		prev = _prev;
		next = _next;
	}
};

template <typename T>
class IStore {
public:
	virtual void add(T value) = 0;
	virtual T getCurrent() = 0;
	virtual T removeCurrent() = 0;
	virtual void first() = 0;
	virtual void next() = 0;
	virtual void last() = 0;
	virtual void prev() = 0;
	virtual bool isEol() = 0;
	virtual bool contains(bool (*cmpcb)(T)) = 0;
};

template <typename T>
class Store : public IStore<T> {
	Item<T>* _current;
	Item<T>* _first;
	Item<T>* _last;
	using cmpfn = bool(*)(T);
public:
	Store() {
		_current = nullptr;
		_first = nullptr;
		_last = nullptr;
	}

	void add(T value) override {
		Item<T>* item = new Item<T>(value);
		if (_first == nullptr) {
			// when it's first item
			_first = item;
			_last = item;
			_current = item;
		}
		else {
			// pushing to the end
			_last->next = item;
			item->prev = _last;
			_last = item;
		}
	}

	T removeCurrent() override {
		if (_current == nullptr) return NULL;
		Item<T>* prev = _current->prev;
		Item<T>* next = _current->next;
		T value = _current->value;
		delete _current;

		if (prev == nullptr && next == nullptr) {
			// if deleted element is only one in list
			_first = nullptr;
			_last = nullptr;
		}
		else if (next == nullptr) {
			// if it's last
			_last = prev;
			_last->next = nullptr;
			_current = prev;
		}
		else if (prev == nullptr) {
			// if it's first
			_first = next;
			_first->prev = nullptr;
			_current = next;
		}
		else {
			// if it's in the middle
			prev->next = next;
			next->prev = prev;
			_current = next;
		}

		return value;
	}

	void first() override {
		_current = _first;
	}

	void last() override {
		_current = _last;
	}

	void next() override {
		if (!isEol()) _current = _current->next;
	}

	void prev() override {
		if (!isEol()) _current = _current->prev;
	}

	bool isEol() override {
		return _current == nullptr;
	}

	T getCurrent() override {
		return _current->value;
	}

	bool contains(cmpfn fn) override {
		first();
		while (!isEol()) {
			if (!fn(_current->value)) { next(); continue; }
			return true;
		}
		first();
		return false;
	}

	~Store() {
		if (_first == nullptr) return;
		first();

		while (_current != nullptr) {
			Item<T>* next = _current->next;
			delete _current;
			_current = next;
		}
	}
};

int main() {
	Store<SomeObject*> store = Store<SomeObject*>();

	for (int i = 0; i < 10; i++) {
		store.add(new SomeObject(i));
	}

	cout << "Current state: ";
	for (store.first(); !store.isEol(); store.next()) {
		cout << store.getCurrent()->get() << " ";
	}
	cout << endl;

	store.first();
	SomeObject* removedFirstItem = store.removeCurrent();
	cout << "Removed first item: " << removedFirstItem->get() << endl;
	delete removedFirstItem;
	
	cout << "Current state: ";
	for (; !store.isEol(); store.next()) {
		cout << store.getCurrent()->get() << " ";
	}
	cout << endl;

	store.first();
	for (int i = 0; !store.isEol() && i < 4; store.next(), i++);
	SomeObject* removedMiddleItem = store.removeCurrent();
	cout << "Removed middle item: " << removedMiddleItem->get() << endl;
	delete removedMiddleItem;

	cout << "Current state: ";
	for (store.first(); !store.isEol(); store.next()) {
		cout << store.getCurrent()->get() << " ";
	}
	cout << endl;

	store.last();
	SomeObject* item = store.getCurrent();
	item->set(50);
	cout << "Last item is changed to " << item->get() << endl;

	cout << "Does item with value " << item->get() << " exists: " 
		<< store.contains([](SomeObject* x) -> bool {return x->get() == 50; }) << endl;

	cout << "Current state: ";
	for (store.first(); !store.isEol(); store.next()) {
		cout << store.getCurrent()->get() << " ";
	}
	cout << endl;

	for (store.first(); !store.isEol(); store.next()) {
		delete store.getCurrent();
	}
}